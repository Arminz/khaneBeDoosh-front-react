import React from 'react';
import './NavLink.css';
import navLogo from '../../images/logo.png';

export default class NavLink extends React.Component{
    render(){
        return(
            <a className="nav-link navHomeIcon cyan" href="/">
                <img src={navLogo} alt="logo" className="navLogo"/>
                <p className="navText">
                    خانه به دوش
                </p>
            </a>
        )
    }
}
