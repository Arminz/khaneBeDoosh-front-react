import Header from "./Header/Header";
import PayForm from "./PayForm/PayForm";
import Footer from "./Footer/Footer";
import NewHouseForm from './NewHouseForm/NewHouseForm';
import FormInput from './FormInput/FormInput';
import HouseDetailForm from './HouseDetailForm/HouseDetailForm'
import HouseCardsList from "./HouseCardsList/HouseCardsList"
import LoginForm from "./LoginForm/LoginForm"

export {Header, PayForm, Footer , NewHouseForm , FormInput, HouseDetailForm, HouseCardsList, LoginForm}
