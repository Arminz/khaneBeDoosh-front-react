import React from "react";
import HouseCard from "../HouseCard/HouseCard";

export default class HouseCardsList extends React.Component {
    render(){
        let cards = [];
        const houses = this.props.houses;
        if (houses !== undefined)
        {
            for (let i = 0; 2 * i + 1 < houses.length; i ++)
            {
                cards.push(
                    <div className={"row justify-content-between "}>
                        {/*<div className={"col-md-6"}>*/}
                            <HouseCard house={houses[2 * i]} className="col-md-5"/>
                        {/*</div>*/}
                        {/*<div className={"col-md-6"}>*/}
                            <HouseCard house={houses[2 * i + 1]} className="col-md-5"/>
                        {/*</div>*/}
                    </div>);
            }
            if (houses.length %2 === 1) {
                cards.push(
                    <div className={"row"}>
                        <HouseCard house={houses[houses.length - 1]} className="col-md-5"/>
                        <div className="col-md-5"/>
                    </div>
                );
            }
        }

        return(
            <div className={"headerSearch "  + this.props.className}>
                {cards}
            </div>
        )
    }

}