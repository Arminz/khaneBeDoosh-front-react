import React from "react";

import './FormInput.css';

export default class FormInput extends React.Component{
    render(){
        return (
            <div>
                <input type={this.props.type == null ? "text" : this.props.type} id="FormInput" name={this.props.name} placeholder={this.props.placeholder} required={this.props.required} pattern={this.props.pattern}/>
            </div>
        );
    }
}