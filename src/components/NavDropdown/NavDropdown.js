import React from "react";

import './NavDropdown.css'
import Button from "../Button/Button";
import getUser from "../../utils/UserService";

export default class NavDropdown extends React.Component{
    // constructor(props){
    //     super(props);
    //     this.state = {};
    // }
    //
    // componentWillMount(){
    //     getUser()
    //         .then((res) =>{
    //             this.setState({user: res.data});
    //         });
    // }
    render(){
        return (
            <div className="dropdown-hoverable">
                {this.props.user && <div className="dropdown-content Card LightCard credit">
                    <div className="username">
                        {this.props.user.name}
                    </div>
                    <div className="creditName darkgray">
                        اعتبار:
                    </div>
                    <div className="creditValue darkgray">
                        {this.props.user.balance} تومان
                    </div>
                    <Button className="bcyan increaseCredit" value="افزایش اعتبار"/>
                </div>}
            </div>
        );
    }
}