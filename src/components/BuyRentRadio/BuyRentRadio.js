import React from "react";

import './BuyRentRadio.css';

export default class BuyRentRadio extends React.Component{
    render(){
    return (
        <div className={this.props.containerClassName}>
            <div className="rentDiv">
                <input type="radio" name="dealType" value="خرید" id="sell" required/>
                <label htmlFor="sell">خرید</label>
            </div>
            <div className="sellDiv">
                <input type="radio" name="dealType" value="اجاره" id="rent"/>
                <label htmlFor="rent">رهن و اجاره</label>
            </div>
        </div>
    );
}
}