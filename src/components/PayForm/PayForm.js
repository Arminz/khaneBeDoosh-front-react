import React from "react";

import ValueInput from "../ValueInput/ValueInput";
import Button from "../Button/Button"

import './PayForm.css'

export default class PayForm extends React.Component{

    render(){
        return (
            <div id="payForm" className="container">
                <div className="row">
                    {this.props.balance != null &&
                    <div id="CurrentCredit" className="col-md-6">
                        اعتبار کنونی <span id="Toman">{this.props.balance}</span> تومان
                    </div>
                    }
                    <div className="col-md-6">
                        <form onSubmit={this.props.onPay}>
                            <ValueInput labelClassName="darkgray" name="balance" label="تومان" placeholder="مبلغ مورد نظر" pattern="[0-9]*" required/>
                            <Button className="bcyan" value="افزایش اعتبار"/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}