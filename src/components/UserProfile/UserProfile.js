import React from "react";
import NavButton from "../NavButton/NavButton";
import NavDropdown from "../NavDropdown/NavDropdown";

import './UserProfile.css'

export default class UserProfile extends React.Component{
    render(){
        return (
            <div className="navigationBar">
                <div className="dropdown">
                    <form action="/pay">
                        <NavButton className="purple"/>
                        <NavDropdown user={this.props.user}/>
                    </form>
                </div>
            </div>
        );
    }
}