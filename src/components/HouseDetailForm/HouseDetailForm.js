import React from "react";
import './HouseDetailForm.css';
import FieldDetail from "../FieldDetail/FieldDetail";
import HouseInfoButton from "../HouseInfoButton/HouseInfoButton";
import Button from "../Button/Button";
import {NotificationContainer, NotificationManager} from "react-notifications";
import 'react-notifications/lib/notifications.css';
import noimage from "../../images/no-pic.jpg";

export default class HouseDetailForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {notEnoughCredit: false};
    }

    buy(houseId): void {
        const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');
        const API = "http://localhost:8080/house/buy?houseId=" + houseId;
        fetch(API , {
            method: 'post',
            mode: "no-cores",
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8',
                'Authorization': bearer
            }),
        })
            .then(function(response) {
                if (response.status === 200) {
                    response.json().then(
                        (response) => NotificationManager.success(response.message)
                    );
                    this.props.refreshHouse();
                }
                else if (response.status === 402) {
                    this.setState({notEnoughCredit: true});
                }
                else {
                    response.json().then(
                        (response) => NotificationManager.error(response.msg)
                    );
                }
            }.bind(this))
            .catch(error => console.log(error.message));
    }
    render() {
        if (this.props.house == null) {
            return (
                <div>
                    خانه مورد نظر یافت نشد!
                </div>
            )
        }
        else {
            const house = this.props.house;
            console.log("house: " + JSON.stringify(house));
            // const price = house.dealType===0 ? (
            //         <FieldDetail label="قیمت" value={house.price.sellPrice + " تومان"}/>
            //     ) :
            //     [
            //         <FieldDetail label="رهن" value={house.price.basePrice + " تومان"}/>,
            //         <FieldDetail label="اجاره" value={house.price.rentPrice + " تومان"}/>
            //     ];
            return (
                <div className={this.props.className}>
                    <div className={"row"}>
                        <div className={"col-lg-6"}>
                            <HouseInfoButton dealType={house.dealType} className={"detailHouseInfoButton"}/>
                            <table className={"table DetailFormTable"}>
                                <tbody>
                                    <FieldDetail label="شماره مالک/مشاور" value={house.phone}/>
                                    <FieldDetail label="نوع ساختمان" value={house.buildingType}/>
                                    {house.dealType===0 ?
                                        <FieldDetail label="قیمت" value={house.price.sellPrice + " تومان"}/> :
                                        [<FieldDetail label="رهن" value={house.price.basePrice + " تومان"}/>,
                                        <FieldDetail label="اجاره" value={house.price.rentPrice + " تومان"}/>]
                                    }
                                    <FieldDetail label="آدرس" value={house.address}/>
                                    <FieldDetail label="متراژ" value={house.area + "متر مربع"}/>
                                    <FieldDetail label="توضیحات" value={house.description}/>
                                </tbody>
                            </table>
                        </div>
                        <div className={"col-lg-6"}>
                            <img src={(house.imageURL == "/static/no-pic.jpg") ? noimage : house.imageURL} className={"HouseDetailImage"} alt="house"/>
                            {!this.state.notEnoughCredit && !house.isBought && <Button className="bcyan houseDetailButton" value={"مشاهده شماره مالک/مشاور"} onClick={() => this.buy(house.id)}/>}
                            {this.state.notEnoughCredit && <Button className="byellow houseDetailButton" value={"اعتبار شما برای مشاهده شماره مالک/مشاور این ملک کافی نیست"} onClick={() => this.buy(house.id)}/>}
                        </div>
                    </div>
                    <NotificationContainer/>
                </div>
            )
        }
    }
}