import React from 'react';
import './NewHouseForm.css';
import ValueInput from "../ValueInput/ValueInput";
import FormInput from "../FormInput/FormInput";
import Button from "../Button/Button";
import BuyRentRadio from "../BuyRentRadio/BuyRentRadio";

export default class NewHouseForm extends React.Component{
    constructor(props){
        super(props);
        this.submit = this.submit.bind(this);
    }

    submit(event){
        event.preventDefault();
        const data = {
            dealType: event.target.dealType.value,
            buildingType: event.target.buildingType.value,
            area: event.target.area.value,
            address: event.target.address.value,
            sellPrice: (event.target.sellPrice.value === "") ? 0:event.target.sellPrice.value,
            phone: event.target.phone.value,
            rentPrice: (event.target.rentPrice.value === "") ? 0:event.target.rentPrice.value,
            description: event.target.description.value
        };
        this.props.submit(data);
    }

    render(){
        return(
            <div className="container" id="NewHouseForm">
                <form onSubmit={this.submit}>
                    <div className="row formRow">
                        <BuyRentRadio containerClassName="col-md-6"/>
                    </div>
                    <div className="row formRow justify-content-between">
                        <div className="col-md-6">
                            <select className="dealTypeSelect" name="buildingType" required>
                                <option value="" disabled selected hidden>نوع ملک</option>
                                <option value="آپارتمان">آپارتمان مسکونی</option>
                                <option value="ویلایی">خانه ویلایی</option>
                            </select>
                        </div>
                        <div className="col-md-6">
                            <ValueInput labelClassName="darkgray" name="area" placeholder="متراژ" label="متر مربع" required pattern="[0-9]*"/>
                        </div>
                    </div>
                    <div className="row formRow">
                        <div className="col-md-6">
                            <FormInput name="address" placeholder="آدرس" required/>
                        </div>
                        <div className="col-md-6">
                            <ValueInput labelClassName="darkgray" name="sellPrice" placeholder="قیمت رهن" label="تومان" pattern="[0-9]*"/>
                        </div>
                    </div>
                    <div className="row formRow">
                        <div className="col-md-6">
                            <FormInput name="phone" placeholder="شماره تماس" required pattern="[0-9]+"/>
                        </div>
                        <div className="col-md-6">
                            <ValueInput labelClassName="darkgray" name="rentPrice" placeholder="قیمت اجاره" label="تومان" pattern="[0-9]*"/>
                        </div>
                    </div>
                    <div className="row formRow">
                        <div className="col-md-12">
                            <FormInput name="description" placeholder="توضیحات" />
                        </div>
                    </div>
                    <div className="row formRow justify-content-end">
                        <div className="col-md-4">
                            <Button className="bcyan" value="ثبت ملک"/>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}