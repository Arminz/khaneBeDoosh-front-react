import React from "react";
import './Footer.css';
import instagram from '../../images/instagram.png';
import telegram from '../../images/telegram.png';
import twitter from '../../images/twitter.png';

export default class Footer extends React.Component{
    render(){
        return (
            <footer className={this.props.className + " container-fluid"}>
                <div className="Footer">
                    <div className="row justify-content-between">
                        <div className="col-md-8 RightsReserved">
                            تمامی حقوق مادی و معنوی این وب سایت متعلق به آرمین زیرک و امیرمحمد کریمی می باشد
                        </div>
                        <div className="col-md-4 footerIcon">
                            <img src={instagram} className="miniIcons" alt="instagram"/>
                            <img src={telegram} className="miniIcons" alt="telegram"/>
                            <img src={twitter} className="miniIcons" alt="twitter"/>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}