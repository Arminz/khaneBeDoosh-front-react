import React from "react";

import './SearchForm.css';
import ValueInput from "../ValueInput/ValueInput";
import BuyRentRadio from "../BuyRentRadio/BuyRentRadio";
import Button from "../Button/Button";

export default class SearchForm extends React.Component{
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);

    }
    onSubmit(event){
        if (this.props.onSubmit === undefined)
            return;
        event.preventDefault();
        const data = {
            buildingType: event.target.buildingType.value,
            maxPrice: event.target.maxPrice.value,
            minArea: event.target.minArea.value,
            dealType: event.target.dealType.value
        };
        this.props.onSubmit(data);
    }
    render(){
        return (
            <div className="Card BlackCard">
                <form onSubmit={this.onSubmit} method="get" action={'/house'}>
                    <div className="row">
                        <div className="col-md-4">
                            <select className="dealTypeSelect" name="buildingType" required>
                                <option value="" disabled selected hidden>نوع ملک</option>
                                <option value="آپارتمان">آپارتمان مسکونی</option>
                                <option value="ویلایی">خانه ویلایی</option>
                            </select>
                        </div>
                        <div className="col-md-4">
                            <ValueInput labelClassName="white" label="تومان" name="maxPrice" placeholder="حداکثر قیمت"/>
                        </div>
                        <div className="col-md-4">
                            <ValueInput labelClassName="white" label="مترمربع" name="minArea" placeholder="حداقل متراژ"/>
                        </div>
                    </div>
                    <div className="row justify-content-between">
                        <BuyRentRadio containerClassName="col-md-4 DealTypeRadio white"/>
                        <div className="col-md-5">
                            <Button className="bcyan" value="جست و جو"/>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}