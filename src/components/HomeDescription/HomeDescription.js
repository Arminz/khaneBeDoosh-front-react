import React from "react";

import './HomeDescription.css';
import whyKhaneBeDoosh from "../../images/why-khanebedoosh.jpg";


export default class HomeDescription extends React.Component{
    render(){
        return (
            <div className="container" id="bodyContainer">
                <p id="whyKhaneBeDooshTitle">
                    چرا خانه به دوش؟
                </p>
                <div className="row justify-content-between">
                    <div className="col-md-6">
                        <ul>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                اطلاعات کامل و صحیح از املاک قابل معامله
                            </li>
                            <li >
                                <i className="fa fa-check-circle ticks"/>
                                بدون محدودیت, ۲۴ ساعته و در تمام ایام هفته
                            </li>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                جست و جوی هوشمند ملک, صرفه جویی در زمان
                            </li>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                تنوع در املاک, افزایش قدرت خرید مشتریان
                            </li>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                بانکی جامع از اطلاعات هزاران آگهی به روز
                            </li>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                دستیابی به نتیجه مطلوب در کمترین زمان ممکن
                            </li>
                            <li>
                                <i className="fa fa-check-circle ticks"/>
                                همکاری با مشاوران متخصص در حوزه املاک
                            </li>
                        </ul>

                    </div>
                    <div className="col-md-4 why-image">
                        <img src={whyKhaneBeDoosh} id="why-image"/>
                    </div>
                </div>
            </div>
        );
    }
}