import React from "react";
import NavBar from "../NavBar/NavBar";
import TitleBar from "../TitleBar/TitleBar";

import './Header.css'


export default class Header extends React.Component{
    render(){
        return(
            <div>
                <NavBar user={this.props.user}/>
                <TitleBar title={this.props.title}/>
            </div>
        );
    }
}