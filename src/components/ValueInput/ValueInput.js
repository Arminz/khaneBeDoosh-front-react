import React from "react";

import './ValueInput.css'

export default class ValueInput extends React.Component{
    render(){
        return (
            <div>
                <p className={"Label " + this.props.labelClassName}>{this.props.label}</p>
                <input type="text" name={this.props.name} placeholder={this.props.placeholder} required={this.props.required} pattern={this.props.pattern} defaultValue={this.props.defaultValue}/>
            </div>
        );
    }
}