import React from "react";

import './HomeCard.css';

export default class HomeCard extends React.Component{
    render(){
        return (
            <div className="Card LightCondensed">
                <img src={this.props.imageSrc} className="IndexCardImg"/>
                <p className="CardTitle">
                    {this.props.title}
                </p>
                <p>
                    {this.props.text}
                </p>
            </div>
        );
    }
}