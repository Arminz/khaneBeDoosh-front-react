import React from "react";

import './HomeLogo.css';
import logo from "../../images/logo.png";

export default class HomeLogo extends React.Component{
    render(){
        return (
            <div className="row">
                <div className="col-md-4"></div>
                <div className="col-md-4 headerIcon">
                    <a href="/">
                        <img src={logo} alt="logo" className="logo"/>
                    </a>
                    <p className="headerTitle white">
                        خانه به دوش
                    </p>
                </div>
                <div className="col-md-4"></div>
            </div>
        );
    }
}