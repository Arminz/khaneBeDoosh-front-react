import React from "react";

import './HomeContent.css';
import HomeLogo from "../HomeLogo/HomeLogo";
import SearchForm from "../SearchForm/SearchForm";
import NewHouseButton from "../NewHouseButton/NewHouseButton";
import HomeCardContainer from "../HomeCardContainer/HomeCardContainer";
import Redirect from "react-router-dom/es/Redirect";

export default class HomeContent extends React.Component{

    state = {toHouseList: undefined};

    constructor(props){
        super(props);
    }
    loadHouseList = (data) => {
        // this.setState({toHouseList: data});
    };

    render(){
        if (this.state.toHouseList !== undefined)
            return <Redirect to={'/house'} houses={this.state.toHouseList}/>;

        return (
            <div className="container headerContainer">
                <HomeLogo/>
                <SearchForm/>
                <NewHouseButton/>
                <HomeCardContainer/>
            </div>
        );
    }
}