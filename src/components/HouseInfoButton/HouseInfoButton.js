import React from 'react';
import './HouseInfoButton.css';

export default class HouseInfoButton extends React.Component{
    constructor(props){
        super();
        this.colorClass = props.dealType===0?"bpurple":"bred";
        this.buttonText = props.dealType===0?"فروش":"رهن و اجاره";
    }
    render(){
        return(
            <div className={"Button " + (this.props.positionalClass!==undefined?this.props.positionalClass:"") + " " +  this.colorClass + " " + this.props.className}>{this.buttonText}</div>
        )
    }
}
