import React from "react";
import './FieldDetail.css'

export default class FieldDetail extends React.Component{
    render(){
        console.log("t:" + this.props.label==="توضیحات");
        return (
            <tr>
                <td className={"darkgray fieldDetailLabel"}>{this.props.label}</td>
                <td className={"fieldDetailValue valueTd" + (this.props.label==="شماره مالک/مشاور"?" numberValue":"")  + " " + (this.props.label==="توضیحات"?"fieldDescriptionValue":"")}>{this.props.value}</td>
            </tr>
        )
    }
}