import React from "react";
import NavLink from "../NavLink/NavLink";
import UserProfile from "../UserProfile/UserProfile";

import './NavBar.css'

export default class NavBar extends React.Component{
    render(){
        return (
            <nav className="navbar LightCard navbarContainer">
                <ul className="navbar-nav navigation">
                    <div className="nav-item">
                        <NavLink/>
                    </div>
                </ul>
                <ul className="navbar-nav navbarButtonContainer">
                    <div className="nav-item">
                        <UserProfile user={this.props.user}/>
                    </div>
                </ul>
            </nav>
        );
    }
}