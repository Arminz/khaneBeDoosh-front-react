import React from "react";

import './LoginForm.css';
import FormInput from "../FormInput/FormInput";
import Button from "../Button/Button";

export default class LoginForm extends React.Component{

    constructor(props){
        super(props);
        this.submit = this.submit.bind(this);
    }

    submit(event){
        event.preventDefault();
        const data = {
            username: event.target.username.value,
            password: event.target.password.value
        };
        this.props.submit(data);
    }


    render(){
        return (
            <div className="container" id="LoginForm">
                <form onSubmit={this.submit} method="post">
                    <div className="row formRow justify-content-center">
                        <div className="col-md-2">
                            <p className="loginParagraph darkgray">
                                نام کاربری‌‌ :
                            </p>
                        </div>
                        <div className="col-md-6">
                            <FormInput labelClassName="darkgray" name="username" placeholder="نام کاربری" required/>
                        </div>
                    </div>
                    <div className="row formRow justify-content-center">
                        <div className="col-md-2">
                            <p className="loginParagraph darkgray">
                                رمز عبور :
                            </p>
                        </div>
                        <div className="col-md-6">
                            <FormInput labelClassName="darkgray" name="password" type="password" required/>
                        </div>
                    </div>
                    <div className="row formRow justify-content-center">
                        <div className="col-md-2"/>
                        <div className="col-md-6">
                            <Button className="bcyan" value="ورود"/>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}