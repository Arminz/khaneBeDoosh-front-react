import React from "react";

import './HomeCardContainer.css';
import HomeCard from "../HomeCard/HomeCard";
import img1 from "../../images/726446.svg";
import img2 from "../../images/726488.svg";
import img3 from "../../images/726499.svg";

export default class HomeCardContainer extends React.Component{
    render(){
        return (
            <div className="row" id="cardsRow">
                <div className="col-md-4">
                    <HomeCard  imageSrc={img1} title="آسان" text="به سادگی صاحب خانه شوید"/>
                </div>
                <div className="col-md-4">
                    <HomeCard  imageSrc={img2} title="مطمین" text="با خیال راحت به دنبال خانه بگردید"/>
                </div>
                <div className="col-md-4">
                    <HomeCard  imageSrc={img3} title="گسترده" text="در منطقه مورد علاقه خود صاحب خانه شوید"/>
                </div>
            </div>
        );
    }
}