import React from "react";

import './NavButton.css'
export default class NavButton extends React.Component{
    render(){
        return (
            <button className={"navButton " + this.props.className}>
                <i className="fa fa-smile-o smiley"/>
                ناحیه کاربری
            </button>
        );
    }
}