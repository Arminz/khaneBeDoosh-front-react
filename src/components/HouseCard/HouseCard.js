import React from "react";

import './HouseCard.css';
import noimage from "../../images/no-pic.jpg";

export default class HouseCard extends React.Component{
    render(){
        return (
            <div className={"Card LightCondensed LightCardCondensed " + this.props.className} >
                <a href={"/house/" + this.props.house.id}>
                    <img src={(this.props.house.imageURL == "/static/no-pic.jpg") ? noimage : this.props.house.imageURL} alt="house" className="houseImage"/>
                    <div className={"Button houseInfoButton " + ((this.props.house.dealType == 1) ? "bred" : "bpurple")}>{(this.props.house.dealType == 1) ? "رهن و اجاره" : "فروش"}</div>
                </a>
                <div className="row justify-content-between">
                    <p className="right col-md-5">
                        {this.props.house.area} متر مربع
                    </p>
                    <p className="left col-md-5">
                        <i className="fa fa-map-marker locationIcon"></i>
                        {this.props.house.address}
                    </p>
                </div>
                <hr className="line"/>
                    {/*<div className="row justify-content-between">*/}
                        {(this.props.house.dealType == 1) ?
                            (<div className="row justify-content-between">
                                <div className="col-md-6 ">
                                <p>
                                    رهن
                                </p>
                                <p>
                                    {this.props.house.price.basePrice}
                                </p>
                                <p className="currency">
                                    تومان
                                </p>
                                </div>
                                <div className="col-md-6">
                                <p>
                                    اجاره
                                </p>
                                <p>
                                    {this.props.house.price.rentPrice}
                                </p>
                                <p className="currency">
                                    تومان
                                </p>
                                </div>
                            </div>) :
                                (<div className="row justify-content-between">
                                    <div className="col-md-6 ">
                                        <p>
                                            قیمت
                                        </p>
                                        <p>
                                            {this.props.house.price.sellPrice}
                                        </p>
                                        <p className="currency">
                                            تومان
                                        </p>
                                    </div>
                                    <div className="col-md-6"></div>
                                </div>)}
                    {/*</div>*/}
            </div>
        );
    }
}