import React from "react";

import './HomeBackground.css';
import bi1 from "../../images/bi1.jpg";
import bi2 from "../../images/bi2.jpg";
import bi3 from "../../images/bi3.jpg";
import bi4 from "../../images/bi4.jpg";

export default class HomeBackground extends React.Component{
    render(){
        return (
            <div className="cf">
                <div>
                    <img src={bi1} />
                    <img src={bi2} />
                    <img src={bi3} />
                    <img src={bi4} />
                </div>
            </div>
        );
    }
}