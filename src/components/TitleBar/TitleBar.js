import React from "react";

import './TitleBar.css'

export default class TitleBar extends React.Component{
    render(){
        return (
            <nav className="navbar navbar-expand-md LightCard header">
                <ul className="navbar-nav nav2">
                    <li className="nav-item">
                        <div className="navHomeIcon cyan">
                            <p className="nav2Text">
                                {this.props.title}
                            </p>
                        </div>
                    </li>
                </ul>
            </nav>
        );
    }
}