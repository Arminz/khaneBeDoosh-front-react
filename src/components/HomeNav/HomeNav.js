import React from "react";

import './HomeNav.css';
import './../Button/Button.css';
import NavButton from "../NavButton/NavButton";
import NavDropdown from "../NavDropdown/NavDropdown";

export default class HomeNav extends React.Component{
    render(){
        return (
            <div className="homeNav container-fluid">
                <div className="navigationBar">
                    <div className="dropdown">
                        <form action="/pay">
                            <NavButton className="white homeButton"/>
                            <NavDropdown user={this.props.user}/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}