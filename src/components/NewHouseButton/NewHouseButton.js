import React from "react";

import './NewHouseButton.css';
import './../Button/Button.css';

export default class NewHouseButton extends React.Component{
    render(){
        return (
            <div className={"Card BlackCard row registerButton " + this.props.className}>
                <a href="/NewHouse" className="col-md-12 white">صاحب خانه هستید؟ خانه خود را ثبت کنید</a>
            </div>
        );
    }
}