
export default function getHouses(minArea, maxPrice, buildingType, dealType) {
    let userApi = "http://localhost:8080/house";
    let queryParams = [];
    if (minArea !== undefined)
        queryParams.push("minArea=" + minArea);
    if (maxPrice !== undefined)
        queryParams.push("maxPrice=" + maxPrice);
    if (buildingType !== undefined)
        queryParams.push("buildingType=" + buildingType);
    if (dealType !== undefined)
        queryParams.push("dealType=" + dealType);
    if (queryParams.length > 0) {
        let query = queryParams.join('&');
        userApi += "?" + (query)
    }
    const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');

    return fetch(userApi , {
        method: 'get',
        mode: "no-cores",
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8'}),
            'authorization': bearer
    }).then((response) => response.json());
}