import ReactDOM from "react-dom";
import React from "react";
// import "./lib/css/bootstrap.min.css";
import 'font-awesome/css/font-awesome.min.css';
import './lib/css/bootstrap.min.css'
import './style.css'
import Route from "react-router-dom/es/Route";
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import Pay from './containers/Pay/Pay'
import NewHouse from "./containers/NewHouse/NewHouse";
import DetailHouse from "./containers/HouseDetail/HouseDetail";
import Home from "./containers/Home/Home";
import HouseList from "./containers/HouseList/HouseList";
import Login from "./containers/Login/Login";
// ReactDOM.render(
//     <div id='payBody'>
//         <Header title="افزایش اعتبار"/>
//         <PayForm/>
//         <Footer className="SmallPageFooter"/>
//     </div>,
//     document.getElementById('root')

// );

ReactDOM.render(
        <BrowserRouter>
            <div>
                <Route exact path="/pay" component={Pay}/>
                <Route exact path="/NewHouse" component={NewHouse}/>
                <Route exact path="/house/:houseId" component={DetailHouse}/>
                <Route exact path="/" component={Home}/>
                <Route exact path="/house" component={HouseList}/>
                <Route exact path="/login" component={Login}/>
            </div>
        </BrowserRouter>
        , document.getElementById('root')
);


