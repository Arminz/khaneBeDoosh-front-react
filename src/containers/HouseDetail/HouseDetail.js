import React from "react";
import {Footer, Header, HouseDetailForm} from "../../components/index";
import {NotificationContainer, NotificationManager} from "react-notifications";
import loadUser from "../../utils/UserService";

export default class HouseDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};

    }
    componentDidMount(){
        this.loadHouse(this.props.match.params.houseId);
        loadUser()
            .then((res) => {
                this.setState({
                    balance: res.data.balance,
                    name: res.data.name
                })
            });
    }
    loadHouse(houseId){
        const API = "http://localhost:8080/house/detail?houseId=" + houseId;
        const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');
        fetch(API , {
            method: 'get',
            mode: "no-cores",
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8',
                'authorization': bearer
            }),
        })
            .then((response) => {
                if (response.status === 404)
                    NotificationManager.error("یافت نشد!");
                return response.json()
                }
            )
            .then((resp) => this.setState({house: resp.data}))
            .catch(error => console.log(error));
    }
    reload(houseId){
        this.loadHouse(houseId);
        loadUser()
            .then((res) => this.setState({
                balance: res.data.balance,
                name: res.data.name
            }));
    }
    render(){
        return (
            <div>
                <Header title="مشخصات کامل ملک" user={this.state}/>
                {this.state.house && <HouseDetailForm className="container MarginTopAndBottom" house={this.state.house} refreshHouse={() => this.reload(this.props.match.params.houseId)}/>}
                <Footer className="SmallPageFooter"/>
                <NotificationContainer/>
            </div>

        )
    }
}