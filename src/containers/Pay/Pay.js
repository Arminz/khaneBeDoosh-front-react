import React from "react";
import {Footer, Header, PayForm} from "../../components/index"
import qs from "qs";
import {NotificationContainer, NotificationManager} from "react-notifications";

import loadUser from "../../utils/UserService";
import Redirect from "react-router-dom/es/Redirect";

export default class Pay extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
        this.pay = this.pay.bind(this);
    }

    componentDidMount(){
        var loaded = false;
        loadUser()
            .then((res) => {
                loaded = true;
                if (res.data !== undefined){
                    this.setState({
                        balance: res.data.balance,
                        name: res.data.name
                    });
                }else {
                    NotificationManager.error('باید وارد شوید');
                }
                });
    }


    pay(event){
        event.preventDefault();
        const API = "http://localhost:8080/user/pay";
        const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');

        fetch(API , {
            method: 'post',
            mode: "no-cores",
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8',
                'authorization': bearer
            }),
            body: qs.stringify({balance: event.target.balance.value})
        })
            .then(function (response) {
                if (response.status === 200)
                    response.json().then(function (res)  {
                        NotificationManager.success(res.msg);
                        this.setState({balance: res.balance});
                    }.bind(this));

                else {
                    response.json().then((res) => NotificationManager.error(res.msg));
                }
            }.bind(this))
            .catch((error) => NotificationManager.error("سرور پاسخگو نمیباشد!"))
    }

    render() {
        if(this.loaded && this.state.balance == null){
            return <Redirect to={"/login"}/>
        }
        return (
            <div id='payBody'>
                <Header title="افزایش اعتبار" user={this.state}/>
                <PayForm onPay={this.pay} balance={this.state.balance}/>
                <Footer className="SmallPageFooter"/>
                <NotificationContainer/>
            </div>
        );
    }
}
