import React from "react";

import {Footer, Header, HouseCardsList} from "../../components/index";
import getHouses from "../../services/HouseService"
import SearchForm from "../../components/SearchForm/SearchForm";
import NewHouseButton from "../../components/NewHouseButton/NewHouseButton";
import 'url-search-params-polyfill';
import {NotificationContainer, NotificationManager} from "react-notifications";


export default class HouseList extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.loadHouses = this.loadHouses.bind(this);
    }

    componentDidMount(){
        const params = new URLSearchParams(this.props.location.search);
        this.loadHouses(
            {
                minArea: params.get('minArea'),
                maxPrice: params.get('maxPrice'),
                dealType: params.get('dealType'),
                buildingType: params.get('buildingType')
        });


    }

    loadHouses(data){
        getHouses(data.minArea, data.maxPrice, data.buildingType, data.dealType)
            .then((res) => {
                if (res.data !== undefined)
                    this.setState({houses: res.data});
                else
                    NotificationManager.error(res.msg);

            });
    }

    render(){
        return (
            <div>
                <Header title="مشخصات خانه ها"/>
                <div className={"container"}>
                    <HouseCardsList houses={this.state.houses} className={"MarginTopAndBottom"}/>
                    <div>
                        <p className={"EndOfPageMarginBottom text-center darkgray SearchAgain"}>
                            جست و جوی مجدد
                        </p>
                    </div>
                    <SearchForm onSubmit={this.loadHouses}/>
                    <NewHouseButton className={"EndOfPageMarginBottom"}/>
                </div>
                <Footer/>
                <NotificationContainer/>
            </div>
        )
    }

}