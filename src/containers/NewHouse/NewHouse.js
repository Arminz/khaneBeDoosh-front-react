import React from "react";
import {Footer, Header, NewHouseForm} from "../../components/index";
import {NotificationContainer, NotificationManager} from "react-notifications";
import qs from "qs";
import 'react-notifications/lib/notifications.css';
import loadUser from "../../utils/UserService";


export default class NewHouse extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    componentDidMount(){
        loadUser()
            .then((res) => this.setState({
                balance: res.data.balance,
                name: res.data.name
            }));
    }

    AddNewHouse(data){
        const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');

        fetch('http://localhost:8080/house' , {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8',
                'authorization': bearer
            }),
            body: qs.stringify(data)
        })
            .then((response) => response.json())
            .then((res) => {
                if(res.msg === "invalid"){
                    return NotificationManager.error('اطلاعات وارد شده معتبر نمیباشد!' , 'خطا' , 3000);
                }
                else if(res.msg === "ok"){
                    return NotificationManager.success('خانه جدید با موفقیت ثبت شد!' , "" , 3000);
                }
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <div>
                <Header title="ثبت ملک جدید در خانه به دوش" user={this.state}/>
                <NewHouseForm submit={this.AddNewHouse}/>
                <Footer className="SmallPageFooter"/>
                <NotificationContainer/>
            </div>
        );
    }
}