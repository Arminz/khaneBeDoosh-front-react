import React from "react";
import {Footer, Header, LoginForm} from "../../components/index"
import qs from "qs";
import {NotificationContainer, NotificationManager} from "react-notifications";

import 'react-notifications/lib/notifications.css';

export default class Login extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
        this.login = this.login.bind(this);
    }

    componentDidMount(){
        // loadUser()
        //     .then((res) => this.setState({
        //         balance: JSON.parse(res.data).balance,
        //         name: JSON.parse(res.data).name
        //     }));
        this.setState({
           balance: 0,
           name: "no user is logged-in"
        });
    }


    login(data){
        const API = "http://localhost:8080/user";
        fetch(API , {
            method: 'post',
            mode: "no-cores",
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8'
            }),
            body: qs.stringify(data)
        })
            .then(function (response) {
                if (response.status === 200)
                    response.json().then(function (res)  {
                        NotificationManager.success(res.msg);
                        localStorage.setItem('token', res.token);
                    }.bind(this));

                else {
                    response.json().then((res) => NotificationManager.error(res.msg));
                }
            }.bind(this))
            .catch((error) => NotificationManager.error("سرور پاسخگو نمیباشد!"))
    }

    render() {
        return (
            <div id='payBody'>
                <Header title="ورود" user={this.state}/>
                <LoginForm submit={this.login} balance={this.state.balance}/>
                <Footer className="SmallPageFooter"/>
                <NotificationContainer/>
            </div>
        );
    }
}
