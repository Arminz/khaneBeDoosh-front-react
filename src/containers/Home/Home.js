import React from "react";
import {Footer} from "../../components/index"
import HomeBackground from "../../components/HomeBackground/HomeBackground";
import HomeNav from "../../components/HomeNav/HomeNav";
import HomeContent from "../../components/HomeContent/HomeContent";
import HomeDescription from "../../components/HomeDescription/HomeDescription";
import loadUser from "../../utils/UserService";

export default class Pay extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
    }

    componentDidMount(){
        loadUser()
            .then((res) => {
                if (res.data !== undefined){
                    this.setState({
                        balance: res.data.balance,
                        name: res.data.name
                    });
                }else {
                    this.setState({
                        balance: "",
                        name: ""
                    });
                }
            });
    }
    render() {
        return (

            <div>
                <HomeBackground/>
                {this.state && <HomeNav user={this.state}/>}
                <HomeContent/>
                <HomeDescription/>
                <Footer/>
            </div>
        );
    }
}
