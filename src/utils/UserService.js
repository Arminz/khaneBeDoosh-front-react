
export default function loadUser() {
    const userApi = "http://localhost:8080/user";
    const bearer = localStorage.getItem('token') && 'Bearer ' + localStorage.getItem('token');
    return fetch(userApi , {
        method: 'get',
        mode: "no-cores",
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset = UTF-8',
            'authorization': bearer
        }),
    })
        .then((response) => {
            if(!response.ok)
                throw Error(response.statusText);
            return response;
        })
        .then(response => response.json())
        .catch((error) => {
            console.log(error);
            return {
                data: JSON.stringify({
                    balance: 0,
                    name: "وارد نشده"
                })
            }
        })
}

